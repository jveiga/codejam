#!/usr/bin/python

N = int(raw_input())

for n in xrange(N):
    C = int(raw_input())
    I = int(raw_input())
    L = map(int, raw_input().split())

    done = False
    i = 0
    while not done:
        P = L[i]
        for j in xrange(i+1, I):
            Q = L[j]
            if P + Q == C:
                res = '{} {}'.format(i+1, j+1)
                done = True
                break
        i += 1

    print 'Case #{}: {}'.format(n+1, res)
