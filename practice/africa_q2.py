N = int(raw_input())

for n in xrange(N):
	L = raw_input().split()
	L.reverse()
	print 'Case #{}: {}'.format(n+1, ' '.join(L))
