#!/usr/bin/python

import math

def get_factors(N):
    factors = []
    for n in xrange(2, N/2 - 1):
        if not N % n:
            factors.append(n)
            N /= n
    return factors


def factorize(p , q):
    if not q % p:
        return 1, q / p
    else:
        # get factors and eliminate commons
        pfac = get_factors(p)

        for fac in pfac:
            if not q % fac:
                p /= fac
                q /= fac

        return p, q


T = int(raw_input())

for t in xrange(T):
    P, Q = map(int, raw_input().split('/'))

    gen = 0

    P, Q = factorize(P, Q)

    ml = math.log(Q, 2)
    if not Q % 2 and ml == int(ml):
        for g in xrange(1, 40):
            if P * 2 ** g >= Q:
                gen = g
                break



    print 'Case #{}: {}'.format(t+1, gen if gen > 0 else 'impossible')
