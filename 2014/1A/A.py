def is_compatible(outlets, devices):
    outlets.sort()
    return outlets == devices

def compute_flips(L):
    return [flip for flip in xrange( 2 ** L)]

def get_answer(i):
    return i if i != -1 else "NOT POSSIBLE"

T = int(raw_input())

for t in xrange(T):
    # Read input, convert strings to int and sort devices
    N, L = map(int, raw_input().split())
    outlets = [int(outlet, 2) for outlet in raw_input().split()]
    devices = [int(device, 2) for device in raw_input().split()]
    devices.sort()

    # Compute all possible flips
    flips = compute_flips(L)

    i = -1
    for flip in flips:
        flipped = [outlet ^ flip for outlet in outlets]
        flipped.sort()

        if is_compatible(flipped, devices):
            i = sum(map(int, [b for b in bin(flip)[2:]]))
            break

    print "Case #{}: {}".format(t+1, get_answer(i))